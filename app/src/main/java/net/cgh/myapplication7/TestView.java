package net.cgh.myapplication7;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

class TestView extends View{
    public float currentX = 100;
    public float currentY = 200;
    Paint p = new Paint();

    public TestView(Context context,AttributeSet attrs) {
        super(context,attrs); }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(Color.WHITE);
        //设置画笔的颜色
        p.setColor(Color.BLACK);
        //绘制一个小球
        canvas.drawCircle(currentX, currentY, 30, p);
        p.setColor(Color.WHITE);
        canvas.drawCircle(currentX-10,currentY-10,5,p);
        p.setStyle(Paint.Style.FILL);
        p.setColor(Color.BLACK);
        canvas.drawRect(300,600,500,800,p);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.currentX = event.getX();
        this.currentY = event.getY();
        //重绘小球
        this.invalidate();
        if (currentX>200&&currentX<500&&currentY>600&&currentY<800){
            System.exit(0);
        }
        return true;
    }

}
